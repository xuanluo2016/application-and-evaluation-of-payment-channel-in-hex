module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      network_id: 1
    },
     ropsten:  {
     network_id: 3,
     host: "localhost",
     port:  8546,
     gas:   29000
   }
  },
   rpc: {
     host: 'localhost',
     post:8080
  },
  mocha: {
    enableTimeouts: false
  }

};
