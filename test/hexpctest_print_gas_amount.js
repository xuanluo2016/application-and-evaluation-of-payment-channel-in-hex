require('truffle-test-utils').init();
const awaitTransactionMined = require ('await-transaction-mined');
const truffleAssert = require('truffle-assertions');
const POLL_INTERVAL = 5000;
const defaultLockedAmount = 5000;
// const defaultDepositAmount = 1e+18; // 1 ETH
const defaultDepositAmount = 1000000; // 1 ETH

var myGasPrice = 100000000000;


// save the test result into a local file
const util = require('util');
const fs = require("fs");
const writeFile = util.promisify(fs.writeFile);
const appendFile = util.promisify(fs.appendFile);

var HEX = artifacts.require('./HEXWithPC.sol');
var Token1 = artifacts.require('./Token1.sol');
var Token2 = artifacts.require('./Token2.sol');
var Token3 = artifacts.require('./Token3.sol');
var Token4 = artifacts.require('./Token4.sol');
var Token5 = artifacts.require('./Token5.sol');
var Token6 = artifacts.require('./Token6.sol');
var Token7 = artifacts.require('./Token7.sol');
var Token8 = artifacts.require('./Token8.sol');
var Token9 = artifacts.require('./Token9.sol');
var Token10 = artifacts.require('./Token10.sol');
var Token11 = artifacts.require('./Token11.sol');
var Token12 = artifacts.require('./Token12.sol');
var Token13 = artifacts.require('./Token13.sol');
var Token14 = artifacts.require('./Token14.sol');
var Token15 = artifacts.require('./Token15.sol');
var Token16 = artifacts.require('./Token16.sol');
var Token17 = artifacts.require('./Token17.sol');
var Token18 = artifacts.require('./Token18.sol');
var Token19 = artifacts.require('./Token19.sol');
var Token20 = artifacts.require('./Token20.sol');
var Token21 = artifacts.require('./Token21.sol');
var Token22 = artifacts.require('./Token22.sol');
var Token23 = artifacts.require('./Token23.sol');
var Token24 = artifacts.require('./Token24.sol');
var Token25 = artifacts.require('./Token25.sol');
var tokensAddress = [];
var gasCostList = [];
var waitingTimeList = [];

var DEBUGMODE = false;

function getAverage(arr){
  // iterate all elements of the array and get the mean by removing the maximum and minimum of the arr
	sum = arr.reduce(function(a, b) { return a + b; });
	if( arr.length <= 2){
		return sum/arr.length;
	}
	max = arr.reduce(function(a, b) { return a > b ? a : b; });
	min = arr.reduce(function(a, b) { return a < b ? a : b; });
	
    avg = (sum -(max + min)) / (arr.length - 2);	
	return avg;
}

contract("HEXWithPC2 Test", async(accounts) => {
  let instance;

  let admin;
  let user;

  // parameters for recording waiting time
  let dateStart;
  let dateEnd;
  let diff; // record the time gap between dateStart and dateEnd

  // parameters for recording gas fee
  let gasCost;

  // parameters related to transations
  let hash;
  let minedTxReceipt;
  let result;
  let resultReceipt;

  //let tokens =[];
  let fileExists;
  let fileName;
  let balance;

  // initialization
   beforeEach(async () => {
      instance = await HEX.deployed();
      admin = accounts[0];
      user = accounts[1];

      // preparae the file to store test results
      fileName = "differentTokens.txt";
      fileExists = await fs.existsSync(fileName);

      // deposit ETH for the admin account

      if (fileExists) {
          try {
             // await appendFile(fileName, "trading time       gas fee(ether)      waiting time(ms)");
              console.log('Successfully find file');
            } catch(e) {
                console.log('Failed to update file');
            }
      } else {
            try {
                await writeFile(fileName, "trading time      gas fee     waiting time(ms) \n");
                console.log('Successfully open file');
            } catch (e) {
                console.log('Failed to create file');
            }
     }

   });


   it("add supported tokens", async() => {
       console.log("add supported tokens");

      // deploy supported tokens, and deposit for the admin: admin
     let hashTest;
     let minedTxReceiptTest;
	 
     let token1 = await Token1.deployed();
     hashTest = await token1.initialize.sendTransaction(user);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken1 = await token1.balanceOf.call(user);
     await token1.approve.sendTransaction(HEX.address, balanceToken1,{from:user}); 
	
	 hashTest = await token1.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     balanceToken1 = await token1.balanceOf.call(admin);
     await token1.approve.sendTransaction(HEX.address, balanceToken1,{from:admin}); 
	 
     let token2 = await Token2.deployed();
     hashTest = await token2.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken2 = await token2.balanceOf.call(admin);
     await token2.approve.sendTransaction(HEX.address, balanceToken2,{from:admin});

     let token3 = await Token3.deployed();
     hashTest = await token3.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken3 = await token3.balanceOf.call(admin);
     await token3.approve.sendTransaction(HEX.address, balanceToken3,{from:admin});

     let token4 = await Token4.deployed();
     hashTest = await token4.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken4 = await token4.balanceOf.call(admin);
     await token4.approve.sendTransaction(HEX.address, balanceToken4,{from:admin});
	 
     let token5 = await Token5.deployed();
     hashTest = await token5.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken5 = await token5.balanceOf.call(admin);
     await token5.approve.sendTransaction(HEX.address, balanceToken5,{from:admin});   
     console.log("tokenAddress.push tokens");

     let token6 = await Token6.deployed();
     hashTest = await token6.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken6 = await token6.balanceOf.call(admin);
     await token6.approve.sendTransaction(HEX.address, balanceToken6,{from:admin});

     let token7 = await Token7.deployed();
     hashTest = await token7.initialize.sendTransaction(admin);
     minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
     let balanceToken7 = await token7.balanceOf.call(admin);
     await token7.approve.sendTransaction(HEX.address, balanceToken7,{from:admin});

      let token8 = await Token8.deployed();
      hashTest = await token8.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken8 = await token8.balanceOf.call(admin);
      await token8.approve.sendTransaction(HEX.address, balanceToken8,{from:admin});

      let token9 = await Token9.deployed();
      hashTest = await token9.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken9 = await token9.balanceOf.call(admin);
      await token9.approve.sendTransaction(HEX.address, balanceToken9,{from:admin});

      let token10 = await Token10.deployed();
      hashTest = await token10.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken10 = await token10.balanceOf.call(admin);
      await token10.approve.sendTransaction(HEX.address, balanceToken10,{from:admin});

      let token11 = await Token11.deployed();
      hashTest = await token11.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken11 = await token11.balanceOf.call(admin);
      await token11.approve.sendTransaction(HEX.address, balanceToken11,{from:admin});

      let token12 = await Token12.deployed();
      hashTest = await token12.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken12 = await token2.balanceOf.call(admin);
      await token12.approve.sendTransaction(HEX.address, balanceToken12,{from:admin});

      let token13 = await Token13.deployed();
      hashTest = await token13.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken13 = await token3.balanceOf.call(admin);
      await token13.approve.sendTransaction(HEX.address, balanceToken13,{from:admin});

      let token14 = await Token14.deployed();
      hashTest = await token14.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken14 = await token4.balanceOf.call(admin);
      await token14.approve.sendTransaction(HEX.address, balanceToken14,{from:admin});

      let token15 = await Token15.deployed();
      hashTest = await token15.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken15 = await token15.balanceOf.call(admin);
      await token15.approve.sendTransaction(HEX.address, balanceToken15,{from:admin});

      let token16 = await Token16.deployed();
      hashTest = await token16.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken16 = await token16.balanceOf.call(admin);
      await token16.approve.sendTransaction(HEX.address, balanceToken16,{from:admin});

      let token17 = await Token17.deployed();
      hashTest = await token17.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken17 = await token17.balanceOf.call(admin);
      await token17.approve.sendTransaction(HEX.address, balanceToken17,{from:admin});

      let token18 = await Token18.deployed();
      hashTest = await token18.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken18 = await token18.balanceOf.call(admin);
      await token18.approve.sendTransaction(HEX.address, balanceToken18,{from:admin});

      let token19 = await Token19.deployed();
      hashTest = await token19.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken19 = await token19.balanceOf.call(admin);
      await token19.approve.sendTransaction(HEX.address, balanceToken19,{from:admin});

      let token20 = await Token20.deployed();
      hashTest = await token20.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken20 = await token20.balanceOf.call(admin);
      await token20.approve.sendTransaction(HEX.address, balanceToken20,{from:admin});

      let token21 = await Token21.deployed();
      hashTest = await token21.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken21 = await token21.balanceOf.call(admin);
      await token21.approve.sendTransaction(HEX.address, balanceToken21,{from:admin});

      let token22 = await Token22.deployed();
      hashTest = await token22.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken22 = await token22.balanceOf.call(admin);
      await token22.approve.sendTransaction(HEX.address, balanceToken22,{from:admin});

      let token23 = await Token23.deployed();
      hashTest = await token23.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken23 = await token23.balanceOf.call(admin);
      await token23.approve.sendTransaction(HEX.address, balanceToken23,{from:admin});

      let token24 = await Token24.deployed();
      hashTest = await token24.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken24 = await token24.balanceOf.call(admin);
      await token24.approve.sendTransaction(HEX.address, balanceToken24,{from:admin});

      let token25 = await Token25.deployed();
      hashTest = await token25.initialize.sendTransaction(admin);
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      let balanceToken25 = await token25.balanceOf.call(admin);
      await token25.approve.sendTransaction(HEX.address, balanceToken25,{from:admin});
	 
     tokensAddress.push(Token1.address);
     tokensAddress.push(Token2.address);
     tokensAddress.push(Token3.address);
     tokensAddress.push(Token4.address);
     tokensAddress.push(Token5.address);
     tokensAddress.push(Token6.address);
     tokensAddress.push(Token7.address);
     tokensAddress.push(Token8.address);
     tokensAddress.push(Token9.address);
     tokensAddress.push(Token10.address);
     tokensAddress.push(Token11.address);
     tokensAddress.push(Token12.address);
     tokensAddress.push(Token13.address);
     tokensAddress.push(Token14.address);
     tokensAddress.push(Token15.address);
     tokensAddress.push(Token16.address);
     tokensAddress.push(Token17.address);
     tokensAddress.push(Token18.address);
     tokensAddress.push(Token19.address);
     tokensAddress.push(Token20.address);
     tokensAddress.push(Token21.address);
     tokensAddress.push(Token22.address);
     tokensAddress.push(Token23.address);
     tokensAddress.push(Token24.address);
     tokensAddress.push(Token25.address);
     // add supported token ETH
     await instance.addSupportedTokens.sendTransaction(0,1);

	let iteration = 3;
	let openPcGasAmount =[];
	for(var i = 0; i < iteration; i++){
		openPcGasAmount[i] = new Array(tokensAddress.length -1);
	}
	let closePcGasAmount =[];
	for(var i = 0; i < iteration; i++){
		closePcGasAmount[i] = new Array(tokensAddress.length -1);
	}
	for (let k = 0; k < iteration; k++){
   // add supported tokens by for loop
   for (let i = 0; i < tokensAddress.length -1; i++){
       console.log("for loop starts");
      //await instance.addSupportedTokens.sendTransaction(tokensAddress[i],1);
      // await instance.depositToken(tokensAddress[i],balanceToken1);
	  await instance.depositToken.sendTransaction(tokensAddress[i+1], defaultDepositAmount,{from: admin, gasPrice: myGasPrice});
	  await instance.depositToken.sendTransaction(tokensAddress[0], defaultDepositAmount,{from: user, gasPrice: myGasPrice});


		console.log("start pc openning");
       /**************openning payment channel ****************************************/
	   let number = i+2;
       let makerBalance = [number];
       let takerBalance = [number];
		
	   makerBalance[0] = defaultLockedAmount;	
	   takerBalance[0] = 0;
       for(let j = 1; j < number; j++){
         makerBalance[j] = 0;
         takerBalance[j] = defaultLockedAmount - makerBalance[j];
       }
	   
	   let tokenAddr = [number]

       for(let j = 0; j < number; j++){
         tokenAddr[j] = tokensAddress[j];
       }
	   //console.log(tokenAddr);

       // ************ creation of signature ******************/ //
       let signature = [];
       let r = [];
       let s = [];
       let v = [];
       var signers = [];
       signers.push(user);
       signers.push(admin);

       let h = await instance.GetSha31.call(signers[0],makerBalance,signers[1],takerBalance, tokenAddr);

        for(let i = 0; i < 2; i++){
         //signature[i] = await web3.eth.sign(signers[i],web3.toHex(msg));
         signature[i] = await web3.eth.sign(signers[i],h);
         signature[i] = signature[i].substr(2);
         r[i] = '0x' + signature[i].slice(0, 64);
         s[i] = '0x' + signature[i].slice(64, 128);
         v[i] =  web3.toDecimal('0x' + signature[i].slice(128, 130)) + 27 ;
        }

        var resultSig = await instance.verify.call(h, signers[0],v[0], r[0], s[0]);
        assert.equal(resultSig, true);
        // ************ end of signature ******************/ //
		
		 // record the balance of buyer and seller before trade
         //console.log("/********************the balance of buyer and seller before trade*******************************************/");
         //console.log("user has: ");
         for(let i = 0; i < tokensAddress.length; i++){
            balance = await instance.getTokenAmount.call(tokensAddress[i],{from: user});
            //console.log("Token"+ i + "  " + web3.fromWei(balance.toNumber(), "ether" ));
         }
         //console.log("admin has: ");
         for(let i = 0; i < tokensAddress.length; i++){
           balance = await instance.getTokenAmount.call(tokensAddress[i],{from: admin});
           //console.log("Token"+ i + "  " + web3.fromWei(balance.toNumber(), "ether" ));
         }
        // console.log("/***************************************************************/");
		 

       // send tx
	
	   hash = await instance.createPC.sendTransaction(user, makerBalance,takerBalance, tokenAddr, v, r, s);


       result = await web3.eth.getTransaction(hash);
       resultReceipt = await web3.eth.getTransactionReceipt(hash);

       gasCost = result.gasPrice.mul(resultReceipt.gasUsed).toNumber();
	   console.log("number of tokens locked in pc: " + number)
	   console.log("gas used in pc open: " + resultReceipt.gasUsed)
	   openPcGasAmount[k][i] =  resultReceipt.gasUsed;

       /**************close of payment channel****************************************/
       let pc_id = await instance.getPCId.call({from: user});

       // a test case for a set of continuous trading results for one user
       number = i+2;
       makerBalance = [number];
       takerBalance = [number];

       for(let j = 0; j < number; j++){
         makerBalance[j] = 10*j;
         takerBalance[j] = defaultLockedAmount - makerBalance[j];
       }

       // ************ creation of signature ******************/ //
	   signature = [];
       r = [];
       s = [];
       v = [];
       signers = [];
       signers.push(user);
       signers.push(admin);

       h = await instance.GetSha32.call(pc_id,signers[0],makerBalance,signers[1],takerBalance, tokenAddr);

        for(let i = 0; i < 2; i++){
         //signature[i] = await web3.eth.sign(signers[i],web3.toHex(msg));
         signature[i] = await web3.eth.sign(signers[i],h);
         signature[i] = signature[i].substr(2);
         r[i] = '0x' + signature[i].slice(0, 64);
         s[i] = '0x' + signature[i].slice(64, 128);
         v[i] =  web3.toDecimal('0x' + signature[i].slice(128, 130)) + 27 ;
        }

        resultSig = await instance.verify.call(h, signers[0],v[0], r[0], s[0]);
        assert.equal(resultSig, true);
        // ************ end of signature ******************/ //
       // start recording waiting time
       dateStart = new Date();
       //hash = await instance.withdrawTokenByClosePC.sendTransaction(tokensAddress[i], 5 ,pc_id,user,admin,makerBalance,takerBalance, {from: user});
       hash = await instance.closePC.sendTransaction(pc_id,user,admin,makerBalance,takerBalance,tokenAddr,v,r,s,{from: user, gasPrice: myGasPrice});
       // record the end time
       minedTxReceipt = await awaitTransactionMined.awaitTx(web3, hash, {interval: POLL_INTERVAL,ensureNotUncle: false});
       dateEnd = new Date();
	   result = await web3.eth.getTransaction(hash);
       resultReceipt = await web3.eth.getTransactionReceipt(hash);
	   console.log("gas used in pc close: " + resultReceipt.gasUsed);
	   closePcGasAmount[k][i] =  resultReceipt.gasUsed;

	   // record the balance of buyer and seller after trade
         
		//console.log("/*********************the balance of buyer and seller after trade******************************************/");
        //console.log("user has: ");
        for(let i = 0; i < tokensAddress.length; i++){
            balance = await instance.getTokenAmount.call(tokensAddress[i],{from: user});
            //console.log("Token"+ i + "  " + web3.fromWei(balance.toNumber(), "ether" ));
        }
        console.log("admin has: ");
        for(let i = 0; i < tokensAddress.length; i++){
          balance = await instance.getTokenAmount.call(tokensAddress[i],{from: admin});
          //console.log("Token"+ i + "  " + web3.fromWei(balance.toNumber(), "ether" ));
        }
        //console.log("/***************************************************************/");
     }	
	}
	
	console.log(openPcGasAmount);
	console.log(closePcGasAmount);
	
	for(let i = 0; i < tokensAddress.length - 1; i++){
		arr = openPcGasAmount.map(function(value,index) { return value[i]; });
		console.log("number of tokens in open pc: "+ i + "  " + getAverage(arr));
	}
	
	for(let i = 0; i < tokensAddress.length - 1; i++){
		arr = closePcGasAmount.map(function(value,index) { return value[i]; });
		console.log("number of tokens in close pc: "+ i + "  " + getAverage(arr));
	}

/*      var result = [];
     let numTokens = 0;
     // save the test results to test file
     if(fileExists){
         for(let i = 0; i < tokensAddress.length; i ++){

             numTokens = i + 2;
             result.push( numTokens + "  " + web3.fromWei(gasCostList[i], "ether") + "  " +  waitingTimeList[i] );
         }
         await appendFile(fileName, result.join("\n"));
     } */

  });
});
