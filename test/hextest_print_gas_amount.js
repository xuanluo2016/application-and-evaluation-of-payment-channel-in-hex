require('truffle-test-utils').init();
var abi = require('ethereumjs-abi')
var BN = require('bn.js')


const awaitTransactionMined = require ('await-transaction-mined');
const truffleAssert = require('truffle-assertions');
const POLL_INTERVAL = 5000;
const defaultDepositAmount = 1e+18; // 1 ETH

var HEX = artifacts.require('./HEX.sol');
var Token1 = artifacts.require('./Token1.sol');
var Token2 = artifacts.require('./Token2.sol');

var myGasPrice = 100000000000;

// save the test result into a local file
const util = require('util');
const fs = require("fs");
const writeFile = util.promisify(fs.writeFile);
const appendFile = util.promisify(fs.appendFile);
var DEBUGMODE = false;


contract("HEX Test", async(accounts) => {
  let instance;

  let admin;
  let maker;
  let taker;

  let makerBalance;
  let takerBalance;

  // parameters for recording gas fee
  let gasCost;

  // parameters related to transations
  let hash;
  let hashTest;
  let minedTxReceipt;
  let result;
  let resultReceipt;

  // parameters for recording waiting time
  let dateStart;
  let dateEnd;
  let diff; // record the time gap between dateStart and dateEnd

  let balanceToken;
  let tokensAddress = [];

  let defaultDepositAmount;

  let accountBalance;
  let fileExists;
  let fileName;

  let loopTimes;
  let waitingTimeList = [];


  // initialization
   beforeEach(async () => {
      instance = await HEX.deployed();
      admin = accounts[0];
      maker = accounts[3];
      taker = accounts[4];

      // deploy supported tokens
      let token1 = await Token1.deployed();
      let token2 = await Token2.deployed();

      // deposit the supported token for the maker in the Token address
      hashTest = await token1.initialize.sendTransaction(maker,{from:maker, gasPrice: myGasPrice});
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});
      // deposit the supported token for the maker in the Token address
      hashTest = await token2.initialize.sendTransaction(taker,{from:taker, gasPrice: myGasPrice});
      minedTxReceiptTest = await awaitTransactionMined.awaitTx(web3, hashTest, {interval: POLL_INTERVAL,ensureNotUncle: false});

      balanceToken =  await token1.balanceOf.call(maker);
      await token1.approve.sendTransaction(HEX.address, balanceToken,{from:maker, gasPrice: myGasPrice});
      await token2.approve.sendTransaction(HEX.address, balanceToken,{from:taker, gasPrice: myGasPrice});

      // add supported tokens
      await instance.addSupportedTokens.sendTransaction(Token1.address,{gasPrice: myGasPrice});
      await instance.addSupportedTokens.sendTransaction(Token2.address,{gasPrice: myGasPrice});
      tokensAddress.push(Token1.address);
      tokensAddress.push(Token2.address);

      makerBalance = 1;
      takerBalance = 1;

      defaultDepositAmount = 10;

      // preparae the file to store test results
      fileName = "hextest-15-totalTime.txt";
      fileExists = await fs.existsSync(fileName);
      if (fileExists) {
          try {
             // await appendFile(fileName, "trading time       gas fee(ether)      waiting time(ms)");
              console.log('Successfully find file');
            } catch(e) {
                console.log('Failed to update file');
            }
      } else {
            try {
                await writeFile(fileName, " waiting time(ms) \n");
                console.log('Successfully open file');
            } catch (e) {
                console.log('Failed to create file');
            }
     }

     loopTimes = 10;
   });

  it(" test deposit, one trade and withdraw ", async() => {

    for(let i = 0; i < loopTimes; i ++){
          /**************start of deposit****************************************/
          await instance.depositToken.sendTransaction(tokensAddress[0], defaultDepositAmount,{from: maker, gasPrice: myGasPrice});

          // record the begin time
          dateStart = new Date();

          // send tx
          hash = await instance.depositToken.sendTransaction(tokensAddress[1], defaultDepositAmount,{from: taker, gasPrice: myGasPrice});

          // record the end time
          minedTxReceipt = await awaitTransactionMined.awaitTx(web3, hash, {interval: POLL_INTERVAL,ensureNotUncle: false});
          dateEnd = new Date();

          diff = (dateEnd - dateStart); //milliseconds interval
          if(DEBUGMODE){
           console.log("total time for deposit is: " + diff);
          }
          result = await web3.eth.getTransaction(hash);
          resultReceipt = await web3.eth.getTransactionReceipt(hash);
	
          gasCost = result.gasPrice.mul(resultReceipt.gasUsed).toNumber();
		  if(DEBUGMODE){
			console.log("transaction fee in deposit is :" + web3.fromWei(gasCost, "ether" ));
		  }
      //   });

      // it("test trade", async() => {
          /**************start of trade****************************************/

          // record the balance of maker and taker before trade
          if(DEBUGMODE){
              console.log("/---------------------------------------------------------------------/");
              console.log("maker has: ");
              for(let i = 0; i < tokensAddress.length; i ++){
                  accountBalance= await instance.getTokenAmount.call(tokensAddress[i],{from: maker});
                  console.log("Token" + i + ": " + web3.fromWei(accountBalance.toNumber(), "ether" ));
              }

              console.log("taker has: ");
              for(let i = 0; i < tokensAddress.length; i ++){
                  accountBalance= await instance.getTokenAmount.call(tokensAddress[i],{from: taker});
                  console.log("Token" + i + ": " + web3.fromWei(accountBalance.toNumber(), "ether" ));
              }
              console.log("/---------------------------------------------------------------------/");
           }

          // start trading
          let tradeTimes = 1;

          let signers = [];
          signers.push(maker);
          signers.push(taker);
          signers.push(admin);

          let v = [];
          let r = [];
          let s = [];
          let sig = [];

          let makerToken = tokensAddress[0];
          let takerToken = tokensAddress[1];

          let h = abi.soliditySHA3(
          [ "address", "uint", "address",  "uint" ],
          [ new BN(makerToken, 16), makerBalance, new BN(takerToken, 16), takerBalance ]).toString('hex');
          h = '0x' + h;


          for(let i = 0; i < 3; i++){
            //sig[i] = await web3.eth.sign(signers[i],web3.toHex(msg));
            sig[i] = await web3.eth.sign(signers[i],h);
            sig[i] = sig[i].substr(2);
            r[i] = '0x' + sig[i].slice(0, 64);
            s[i] = '0x' + sig[i].slice(64, 128);
            v[i] =  web3.toDecimal('0x' + sig[i].slice(128, 130)) + 27 ;
         }
         if(DEBUGMODE){
             console.log(v[0]);
             console.log(v[1]);
             console.log(v[2]);
           }

         if(DEBUGMODE){
            let resultSig = await instance.verify.call(h, signers[0], v[0], r[0], s[0]);
               assert.equal(resultSig, true);

               resultSig = await instance.verify.call(h, signers[1], v[1], r[1], s[1]);
               assert.equal(resultSig, true);

               resultSig = await instance.verify.call(h, signers[2], v[2], r[2], s[2]);
               assert.equal(resultSig, true);
          }

          for(let i = 0; i < tradeTimes; i++){
             if(DEBUGMODE){
                console.log("start trading");
              }
              dateStart = new Date();
              // send tx
              hash = await instance.settleOrder2.sendTransaction(makerToken, makerBalance,takerToken, takerBalance, signers, v, r, s, {from: admin, gasPrice: myGasPrice});

              // record the end time
              minedTxReceipt = await awaitTransactionMined.awaitTx(web3, hash, {interval: POLL_INTERVAL,ensureNotUncle: false});
              dateEnd = new Date();

              if(DEBUGMODE){
                console.log("end trading");
               }

              let temp1 = (dateEnd - dateStart); //milliseconds interval
              let temp2 = temp1 + diff;
              diff = temp2;
              if(DEBUGMODE){
                console.log("total waiting time for one trade is: " + temp1);
              }

              result = await web3.eth.getTransaction(hash);
              resultReceipt = await web3.eth.getTransactionReceipt(hash);			  
			  
              gasCost += result.gasPrice.mul(resultReceipt.gasUsed).toNumber();
			  
			  console.log("gas used in trading transaction" + resultReceipt.gasUsed)
              console.log("cost in trade is: " +  web3.fromWei(result.gasPrice.mul(resultReceipt.gasUsed).toNumber(), "ether" ));
          }

          // record the balance of maker and taker before trade
          if(DEBUGMODE){
              console.log("/---------------------------------------------------------------------/");
              console.log("maker has: ");
              for(let i = 0; i < tokensAddress.length; i ++){
                  accountBalance= await instance.getTokenAmount.call(tokensAddress[i],{from: maker});
                  console.log("Token" + i + ": " + web3.fromWei(accountBalance.toNumber(), "ether" ));
              }

              console.log("taker has: ");
              for(let i = 0; i < tokensAddress.length; i ++){
                  accountBalance= await instance.getTokenAmount.call(tokensAddress[i],{from: taker});
                  console.log("Token" + i + ": " + web3.fromWei(accountBalance.toNumber(), "ether" ));
              }
              console.log("/---------------------------------------------------------------------/");
           }
        //});

      // it("test withdraw", async() => {
          /**************start of withdramakerw ****************************************/
          // record the start time
          dateStart = new Date();

          // send tx
          hash = await instance.withdrawToken.sendTransaction(tokensAddress[1],makerBalance,{from:taker, gasPrice: myGasPrice});

          // record the end time
          minedTxReceipt = await awaitTransactionMined.awaitTx(web3, hash, {interval: POLL_INTERVAL,ensureNotUncle: false});
          dateEnd = new Date();

          let temp1 = (dateEnd - dateStart); //milliseconds interval
          let temp2 = temp1 + diff;
          diff = temp2;
          if(DEBUGMODE){
               console.log("total time for withdraw   " + temp1);
           }

          result = await web3.eth.getTransaction(hash);
          resultReceipt = await web3.eth.getTransactionReceipt(hash);

          gasCost += result.gasPrice.mul(resultReceipt.gasUsed).toNumber();
          if(DEBUGMODE){
            console.log( "total gas fee for deposit, trade and withdraw: " + web3.fromWei(gasCost, "ether" ) + "ether");
            console.log("total waiting time for deposit, trade and withdraw: " + diff + " ms");
           }
           console.log(diff);
           console.log("gascost is :" + web3.fromWei(gasCost, "ether" ));
           waitingTimeList.push(diff);

     }

     // save the test results to test file
     if(fileExists){
         await appendFile(fileName, waitingTimeList.join("\n"));
     }
  });
});
