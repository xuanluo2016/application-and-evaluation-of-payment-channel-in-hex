pragma solidity ^0.4.24;
import "./Token.sol";
import "./Token1.sol";

contract HEXWithPC2 is SafeMath {
    address public admin; //the admin address
    address[] supportedTokens; // starts with 0, which means ETH

    uint256 numChannels;

    mapping (address => mapping (address => uint)) public tokens; //mapping of token addresses to mapping of account balances (token=0 means Ether)
    mapping (address => mapping (address => uint)) public availableTokens; //record available Tokens of HEX, mapping of token addresses to mapping of account balances (token=0 means Ether)
    mapping (bytes32 => mapping (address => mapping (address => uint))) public lockedTokens; //mapping of channel id to lockedTokens

    mapping (address => uint8) public scaleFactors; // mapping of token addrss to its scale factor compared with ETH

    mapping (address => bytes32) public channelUsers; // mapping of users to their related payment channel ids;
    mapping (bytes32 => Channel) channels; // maping of Payment Channel id to Payment Channel

    enum ChannelStatus{
      Open,
      Challenged,
      Closed
    }

    struct Channel{
      address sender;
      address recipent;
      uint256 challengePeriod;
      uint256 challengePeriodLength;
      uint256 nounce;
      uint256 expire;
      ChannelStatus status;
    }

     event Deposit(address token, address user, uint amount, uint balance);
     event Withdraw(address token, address user, uint amount, uint balance);

     event CreatePC(bytes32 pc_id, address sender);
     event ClosePC(bytes32 pc_id, address sender);

     event Verify(address signer, address sender);

    constructor (address admin_){
        admin = admin_;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin);
        _;
    }

    function getAdmin() returns (address){
        return admin;
    }

    // get the id of the payment channel
    function getPCId() returns (bytes32){
        return channelUsers[msg.sender];
    }

    function getPCStatus(bytes32 id) returns (ChannelStatus) {
        return channels[id].status;
    }

    function deposit() payable {
        tokens[0][msg.sender] = safeAdd(tokens[0][msg.sender], msg.value);
        availableTokens[0][msg.sender] = safeAdd(availableTokens[0][msg.sender],msg.value);
        emit Deposit(0, msg.sender, msg.value, tokens[0][msg.sender]);
    }

    function depositToken(address token, uint amount) {
        //remember to call Token(address).approve(this, amount) or this contract will not be able to do the transfer on your behalf.
        if (token==0) revert();
        if (!Token(token).transferFrom(msg.sender, this, amount)) revert();
        tokens[token][msg.sender] = safeAdd(tokens[token][msg.sender], amount);
        availableTokens[token][msg.sender] = safeAdd(availableTokens[token][msg.sender],amount);
        emit Deposit(token, msg.sender, amount, tokens[token][msg.sender]);
      }

    function withdraw(uint amount) {
        // check if any payment channel is open
        require(channelUsers[msg.sender] == 0x0, 'User has a live channel!');

        // check if enough amount of tokens for withdraw
        if (availableTokens[0][msg.sender] < amount) revert();

        tokens[0][msg.sender] = safeSub(tokens[0][msg.sender], amount);
        availableTokens[0][msg.sender] = safeSub(availableTokens[0][msg.sender], amount);

        if (!msg.sender.call.value(amount)()) revert();
        emit Withdraw(0, msg.sender, amount, tokens[0][msg.sender]);
      }

    function withdrawToken(address token, uint amount) {
        if (token==0) revert();

        // check if any payment channel is open
        require(channelUsers[msg.sender] == 0x0, 'User has a live channel!');

        if (availableTokens[token][msg.sender] < amount) revert();

        tokens[token][msg.sender] = safeSub(tokens[token][msg.sender], amount);
        availableTokens[token][msg.sender] = safeSub(availableTokens[token][msg.sender], amount);

        if (!Token(token).transfer(msg.sender, amount)) revert();

        emit Withdraw(token, msg.sender, amount, tokens[token][msg.sender]);
      }

     // create a payment channel via a two-party signed message
  	// @param: sender represents hex user
  	// @param: token represents the deposited token address by the user
  	// @param: amount represents the number of referred token deposited by the user

  	function createPC(address maker, uint256[] makerBalance, uint256[] takerBalance, address[] tokenAddr, uint8[] v, bytes32[] r, bytes32[] s) returns (bytes32){
  		// only one channel per user
		//require(taker == admin, 'Taker has to be admin');
  		require(channelUsers[maker] == 0x0, 'User already has a live channel!');
        require(makerBalance.length == tokenAddr.length, 'Invalid user balance');
        require(takerBalance.length == tokenAddr.length, 'Invalid admin balance');


        bytes32 hash = sha3(maker, makerBalance, admin, takerBalance, tokenAddr);
        //verify user signature
        require(verify(hash, maker, v[0], r[0],s[0]), "Message was not signed by user");
        //verify admin signature
        require(verify(hash, admin, v[1], r[1],s[1]), "Message was not signed by admin");

  		// create an id for the new channel
  		numChannels++;
  		bytes32 _id = keccak256(now + numChannels);

        //address targetToken;
        uint8 i = 0;

        for(i = 0; i < makerBalance.length; i++){
            address targetToken = tokenAddr[i];
            require(makerBalance[i] >= 0 && takerBalance[i] >= 0, "invalid makerBalance or takerBalance");
            
            lockedTokens[_id][targetToken][maker] = safeAdd(makerBalance[i], 0);
            availableTokens[targetToken][maker] = safeSub(availableTokens[targetToken][maker], makerBalance[i]);             
            
			lockedTokens[_id][targetToken][admin] = safeAdd(takerBalance[i], 0);
  			availableTokens[targetToken][admin] = safeSub(availableTokens[targetToken][admin],takerBalance[i]);

        }

  		// map the newly created channel to the user and to the id of the channel

        Channel memory _channel = Channel(
  			maker,
  			admin,
  			0,
  			3,
  			0,
  			30,
  			ChannelStatus.Open
  			);

  		channels[_id] = _channel;
        channelUsers[maker] = _id;

  		emit CreatePC(_id,maker);
  		return _id;
  	}


    // closePC when both parties agree
    // @param: maker represents traders
    // @param: taker represents the HEx's admin account
    // @param: balance records the token ammouts distributed to the maker and the taker, ordered by the creation time of tokens
    function closePC(bytes32 id, address maker, address taker, uint256[] makerBalance,
         uint256[] takerBalance, address[] tokenAddr, uint8[] v, bytes32[] r, bytes32[] s) public {
      // check if related payment channel for the user exists or not
      require(channelUsers[maker] == id,  "Channel ID does not belongs to the user");
	  require(taker == admin, "taker has to be the admin");

      // check if related input for balance is valid
      require(makerBalance.length == takerBalance.length,"Array length conflicts!");
      require(makerBalance.length == tokenAddr.length, 'Invalid user balance');
      require(takerBalance.length == tokenAddr.length, 'Invalid admin balance');

      bytes32 hash = sha3(id, maker, makerBalance, taker, takerBalance, tokenAddr);

      //verify user signature
      require(verify(hash, maker, v[0], r[0],s[0]), "Message was not signed by user");

      //verify admin signature
      require(verify(hash, taker, v[1], r[1],s[1]), "Message was not signed by admin");

          // release locked funds
      for(uint8 i = 0; i < makerBalance.length ; i++){
            address targetToken = tokenAddr[i];

            // make sure the sum of the tokens from both parties is consistent during the live time of the payment channel
            require(makerBalance[i] >= 0 && takerBalance[i] >= 0, "invalid makerBalance or takerBalance");
            require(makerBalance[i] + takerBalance[i] == lockedTokens[id][targetToken][maker] + lockedTokens[id][targetToken][taker], "input balance does not match with locked token balance");

           // reset the token amounts of the trader
            availableTokens[targetToken][maker] = makerBalance[i];
            tokens[targetToken][maker] = makerBalance[i];

            // reset the token amounts of the admin
            availableTokens[targetToken][taker] = safeAdd(availableTokens[targetToken][taker],takerBalance[i]);
            tokens[targetToken][taker] = availableTokens[targetToken][taker];
      }

      // remove the mapping of between user and the payment channel
      delete channels[id];
      delete channelUsers[maker];
      numChannels--;

      emit ClosePC(id,maker);
    }

    function addSupportedTokens (address token, uint8 scaleFactor) onlyAdmin{
       supportedTokens.push(token);
       scaleFactors[token] = scaleFactor;
     }

     function getAvailableToken (address token) returns(uint256){
       return availableTokens[token][msg.sender];
     }

     function getLockedToken (address token) returns(uint256){
       return tokens[token][msg.sender] - availableTokens[token][msg.sender];
     }

     function getLockedTokenByPCId (bytes32 id, address token) returns(uint256){
       return lockedTokens[id][token][msg.sender];
     }

     function getSuppportedTokenNumber () returns(uint256){
       return supportedTokens.length;
     }

     function getTokenAmount (address token) returns (uint){
         return tokens[token][msg.sender];
     }

     // return the balance of all supported tokens for the msg.sender
     function getBalance() returns (uint[]){
       uint[] balance;
       address token;

       for(uint8 i = 0; i < supportedTokens.length; i++){
         token = supportedTokens[i];
         balance.push(tokens[token][msg.sender]);
       }

       return balance;
     }
        /* @dev check if the provided signature is valid, internal
        * @param hash signed information
        * @param sender signer address
        * @param v sig_v
        * @param r sig_r
        * @param s sig_s
        */
        function verify(bytes32 hash, address sender, uint8 v, bytes32 r, bytes32 s) returns (bool) {
           return ecrecover(keccak256("\x19Ethereum Signed Message:\n32", hash), v, r, s) == sender;
        }

        function GetSha32(bytes32 id, address maker, uint[] makerBalance, address taker, uint[] takerBalance, address[] tokensAddr ) returns (bytes32) {
            return sha3(id, maker, makerBalance, taker, takerBalance, tokensAddr);
        }

        function GetSha31(address maker, uint256[] makerBalance, address taker, uint256[] takerBalance, address[] tokensAddr) returns (bytes32) {
            return sha3(maker, makerBalance, taker, takerBalance, tokensAddr);
        } 
        /* function GetSha31(address maker, uint256[] makerBalance, address taker, uint256[] takerBalance) returns (bytes32) {
            return sha3(maker, makerBalance, taker, takerBalance);
        }*/
}
