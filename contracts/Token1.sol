pragma solidity ^0.4.24;
import "./Token.sol";

contract Token1 is StandardToken, SafeMath{
  function initialize(address account){
    balances[account] = defaultAmount;
  }

  uint256 defaultAmount = 5000000000000;
}

contract Token2 is Token1{

}

contract Token3 is Token1{

}

contract Token4 is Token1{

}

contract Token5 is Token1{

}

contract Token6 is Token1{

}

contract Token7 is Token1{

}

contract Token8 is Token1{

}

contract Token9 is Token1{

}

contract Token10 is Token1{

}

contract Token11 is Token1{

}

contract Token12 is Token1{

}

contract Token13 is Token1{

}

contract Token14 is Token1{

}

contract Token15 is Token1{

}

contract Token16 is Token1{

}

contract Token17 is Token1{

}

contract Token18 is Token1{

}

contract Token19 is Token1{

}

contract Token20 is Token1{

}

contract Token21 is Token1{

}

contract Token22 is Token1{

}

contract Token23 is Token1{

}

contract Token24 is Token1{

}

contract Token25 is Token1{

}
