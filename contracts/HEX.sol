pragma solidity 0.4.24;
import "./Token.sol";

contract HEX is SafeMath {
     address public admin; //the admin address
     address[] supportedTokens; // starts with 0, which means ETH

     mapping (address => mapping (address => uint)) public tokens; //mapping of token addresses to mapping of account balances (token=0 means Ether)

     event Deposit(address token, address user, uint amount, uint balance);
     event DepositToken(address token, address user, uint amount, uint balance);

     event Withdraw(address token, address user, uint amount, uint balance);
     event SettleOrder(address makerToken, address maker, uint makerBalance, address takerToken, address taker, uint takerBalance);


    constructor (address admin_){
        admin = admin_;
    }

    modifier onlyAdmin() {
         require(msg.sender == admin);
         _;
    }

    function getAdmin() returns (address){
        return admin;
    }

    function deposit() payable {
        tokens[0][msg.sender] = safeAdd(tokens[0][msg.sender], msg.value);

        emit Deposit(0, msg.sender, msg.value, tokens[0][msg.sender]);
      }

    function depositToken(address token, uint amount) {
        //remember to call Token(address).approve(this, amount) or this contract will not be able to do the transfer on your behalf.
        if (token==0) revert();
        if (!Token(token).transferFrom(msg.sender, this, amount)) revert();
        tokens[token][msg.sender] = safeAdd(tokens[token][msg.sender], amount);

        emit DepositToken(token, msg.sender, amount, tokens[token][msg.sender]);
      }


    function withdraw(uint amount) {
        if (tokens[0][msg.sender] < amount) revert();
        tokens[0][msg.sender] = safeSub(tokens[0][msg.sender], amount);
        if (!msg.sender.call.value(amount)()) revert();

        emit Withdraw(0, msg.sender, amount, tokens[0][msg.sender]);
      }

    function withdrawToken(address token, uint amount) {
        if (token==0) revert();
        if (tokens[token][msg.sender] < amount) revert();
        tokens[token][msg.sender] = safeSub(tokens[token][msg.sender], amount);
        if (!Token(token).transfer(msg.sender, amount)) revert();

        emit Withdraw(token, msg.sender, amount, tokens[token][msg.sender]);
      }

	// Deprecated, use settleOrder2 instead
     function settleOrder(address maker, address makerToken, uint makerBalance, address takerToken, uint takerBalance,
         address taker, address takerToken2, uint takerBalance2, address makerToken2, uint makerBalance2) onlyAdmin returns (bool) {
        // compare the selling order and buying order, make sure both orders match each other
        require(makerToken == makerToken2);
        require(takerToken == takerToken2);
        require(makerBalance == makerBalance2);
        require(takerBalance == takerBalance2);

        // substract the amount of tokens maker and taker used for the order
        tokens[makerToken][maker] = safeSub(tokens[makerToken][maker],makerBalance);
        tokens[takerToken][taker] = safeSub(tokens[takerToken][taker],takerBalance);

        // add the amount of tokens maker and taker gained for the order
        tokens[takerToken][maker] = safeAdd(tokens[takerToken][maker],takerBalance);
        tokens[makerToken][taker] = safeAdd(tokens[makerToken][taker],makerBalance);

        emit SettleOrder(takerToken,maker,takerBalance,makerToken,taker,makerBalance);

        return true;
     }


     function settleOrder2 (address makerToken, uint makerBalance, address takerToken, uint takerBalance,
          address[] user, uint8[] v, bytes32[] r, bytes32[] s) onlyAdmin returns (bool) {


        // check the validity of parameters
        require(user.length == 3, "invalid traders");
        require(admin == user[2], "order not signed by admin");

        // check the signature from the maker order
        //bytes32 hash = sha256(makerToken, makerBalance, takerToken, takerBalance);
        bytes32 hash = sha3(makerToken, makerBalance, takerToken, takerBalance);
        require(verify(hash, user[0], v[0], r[0], s[0]), "Message not signed by maker");

       // check the signature from the selling order
        require(verify(hash, user[1], v[1], r[1], s[1]), "Message not signed by taker");

        // check the signature from the selling admin
        require(verify(hash, user[2], v[2], r[2], s[2]), "Message not signed by admin");

        address maker = user[0];
        address taker = user[1];

        // substract the amount of tokens maker and taker used for the order
        tokens[makerToken][maker] = safeSub(tokens[makerToken][maker], makerBalance);
        tokens[takerToken][taker] = safeSub(tokens[takerToken][taker], takerBalance);

        // add the amount of tokens maker and taker gained for the order
        tokens[takerToken][maker] = safeAdd(tokens[takerToken][maker], takerBalance);
        tokens[makerToken][taker] = safeAdd(tokens[makerToken][taker], makerBalance);

        emit SettleOrder(makerToken, maker, makerBalance, takerToken, taker, takerBalance);

        return true;
     }

     function getTokenAmount (address token) returns (uint){
         return tokens[token][msg.sender];
     }

     function addSupportedTokens (address token) onlyAdmin{
       supportedTokens.push(token);
     }

     function getSuppportedTokens () returns(uint256){
       return supportedTokens.length;
     }

    /* @dev check if the provided signature is valid, internal
    * @param hash signed information
    * @param sender signer address
    * @param v sig_v
    * @param r sig_r
    * @param s sig_s
    */
    function verify(bytes32 hash, address sender, uint8 v, bytes32 r, bytes32 s) pure returns (bool) {
       return ecrecover(keccak256("\x19Ethereum Signed Message:\n32", hash), v, r, s) == sender;
    }
}
