# Application and Evaluation of Payment Channel in Hybrid Decentralized Ethereum Token Exchange 

Ethereum payment channels allow for off-chain transactions with an on-chain settlement. Parties open one channel with a deposit, continue to sign and verify transactions off-chain, and close the channel with one final transaction, on-chain.

### Setup
Clone the repo and run `npm install`. You will need truffle installed globally


### Install dependent packages
```
npm i web-utils
npm i sleep
npm i await-transaction-mined
npm i truffle-assertions
npm i truffle-test-utils
npm i random-number

```


### Compile & migrate
```
truffle compile
truffle migrate
```

### Run tests
Make sure you have testrpc/Ganache running and listening on port 8545
```
truffle test
truffle test test/hexpctest.js
```
